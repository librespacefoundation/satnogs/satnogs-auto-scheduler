# Changelog

## 0.4 - 2025-01-13

### Changes

#### Container Images

Container images are now available at:
- Docker Hub: `librespace/satnogs-auto-scheduler`
- GitLab: `registry.gitlab.com/librespacefoundation/satnogs/satnogs-auto-scheduler`

Previously, only `master` tagged images were published to Docker Hub. Now both `master` and version-tagged images (e.g. `0.4`) are available.

### Fixes

- Fixed scheduling API error handling to properly handle 404 responses without raising `AttributeError`
  ([ee5babe](https://gitlab.com/librespacefoundation/satnogs/satnogs-auto-scheduler/-/commit/ee5babee46e221684a966da8ad6ac04a836b3d8d))
- Made version command (`--version`) accessible regardless of configuration status
- Fixed version command output when running in Docker container
- Removed progress bar output in non-interactive terminals

### Other Changes

- Updated Python from 3.8 to 3.11
- Modernized packaging: Introduced `pyproject.toml` and changed build backend to [hatchling](https://pypi.org/project/hatchling/)
- Removed vendorized package [Versioneer](https://pypi.org/project/versioneer/) in favor of hatchling's built-in versioning
- Adopted LSF gitlab-ci templates for shared CI maintainance across LSF projects
- Consolidated code style tooling by adopting [ruff](https://github.com/astral-sh/ruff) for linting and formatting,
  replaced flake8, isort, yapf and pylint
- Added [GitLab Code Quality](https://docs.gitlab.com/ee/ci/testing/code_quality.html) scans to show linter results in
  the Merge Request

## 0.3.1 - 2023-11-08

- Removed broken check if the configuration file exists,
  fixes config via env variables (e.g. when using docker)
- Added short developement guide at <docs/development-guide.md>
- Changed error message if API Token is invalid

## 0.3 - 2023-11-04

### Breaking Changes

The SatNOGS DB API Token is always required now. Thus TLEs are fetched from SatNOGS DB diretly
which has multiple benefits. It is much faster than the old method, removes errors when a satellite
entry is using TLEs produced by "new" sources (e.g. SatNOGS Team) and enables output improvements.

### Changes (non exhaustive list)

- Added station-agnostic caching of SatNOGS Netowork transmitter statistics and the SatNOGS DB
  satellite information. Allows re-using the transmitter statistics when sequentially scheduling
  multiple stations. The cache age is determined by the modification timestamp (mtime) in the filesystem.
- Added the usage of satellite names from SatNOGS DB in log output.
- Removed scheduling for satellites with the "frequency misuse" flag in SatNOGS DB.
  Unless the user had special SSA permissions those passes were not scheduled already but made the
  batch-scheduling fail often times. This change follows the
  [change](https://community.libre.space/t/changes-in-network-and-db-for-satellites-that-violate-frequency-regulations/9395)
  in SatNOGS for satellites that violate frequency regulations.
- Removed the deprecated method of fetching TLEs from various sources,
  always fetch TLEs from SatNOGS DB now.
- Enabled scheduling for satellites with temporary NORAD ID (>90000). This makes it obsolete to filter for
  satellite with a maximum norad id.
- Removed the `MAX_NROAD_CAT_ID` setting.
- Added `satnogs_auto_scheduler_setup.py` helper script.
- Added input validation for API Tokens from config.
- Improved log output for scheduling errors.

## 0.2 - 2023-02-06 - 2b77522

First release in 2023.

## v0.1 - 2018-12-02 - 1a844c7

Initial stable release.
